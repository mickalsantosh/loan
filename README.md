Project Setup :- 
    1- Required PHP 7.3^
    2- After Download run composer-update
    3- Run - php artisan migrate //for Database table create
    4- Run - php artisan optimize //for clearing cache and config
    5- Run - php artisan route:clear //for clear route


API Details
BASEURL = http://localhost/task/api

Step 1 :- User registration or Already register you can directly login with email and pin.
        After login OR Registration you get AuthToken, and that AuthToken you need to save, and pass every API where you required.
        
        a -> registration api url - (BASEURL/user-register)
            Fields
            name -> required
            email -> required|unique
            pin -> required|4digit|numeric
        
        b -> login api url - (BASEURL/user-login)
            Fields
            email -> required
            pin -> required
            
Step 2 :- User apply loan API.
        (BASEURL/apply-loan)
        Fields
        AuthToken -> required
        loan_amount -> required|Numeric
        loan_period -> required|Numeric

Step 3 :- User applied loan LIST.
        (BASEURL/apply-loan-list)
        Fields
        AuthToken -> required

Step 4 :- Change loan status.
        (BASEURL/change-loan-status/LoanID) // LoanID take from loan list id and pass as LoanID
        Fields
        AuthToken -> required
        status -> 0|1|2 // you can pas 0 or 1 or 2, i.e. 0=pending, 1=approve, 2=reject 
        
for DB TEST you can directly import database from database/task.sql
admin login id :- admin@admin.com
admin login password|pin :- Admin@123#
        
