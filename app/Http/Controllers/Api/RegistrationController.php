<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class RegistrationController extends Controller {

    public function userRegister(Request $request){
        if(empty($request->name)){
            $this->JsonResponse['Message'] = 'Name required!';
        }elseif(empty($request->email)){
            $this->JsonResponse['Message'] = 'Email required!';
        }elseif(empty($request->pin)){
            $this->JsonResponse['Message'] = 'PIN required!';
        }elseif(!empty($request->email) && !filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            $this->JsonResponse['Message'] = "Email is not valid!";
        }elseif(!empty($request->pin) && strlen($request->pin) > 4) {
            $this->JsonResponse['Message'] = "PIN should be 4 digit!";
        }elseif(!empty($request->pin) && !is_numeric($request->pin)) {
            $this->JsonResponse['Message'] = "PIN should be numeric!";
        }else{
            $validator = Validator::make($request->all(), ['email' => 'unique:users,email']);
            if($validator->fails()){
                $this->JsonResponse['Message'] = "Email already registered!";
            }else{
                $Data = array("name" => $request->name, "email" => $request->email, "password" => Hash::make($request->pin));
                $User = User::create($Data);
                if($User->id){
                    $this->JsonResponse['Status'] = 200;
                    $this->JsonResponse['Message'] = "Success";
                    $this->JsonResponse['Data'] = User::find($User->id)->toArray();
                    $this->JsonResponse['AuthToken'] = $this->encryptDecrypt($User->id);
                }
            }
        }
        return $this->commonMessage($this->JsonResponse);
    }

    public function userLogin(Request $request){
        if(empty($request->email)){
            $this->JsonResponse['Message'] = 'Email required!';
        }elseif(!empty($request->email) && !filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            $this->JsonResponse['Message'] = "Email is not valid!";
        }elseif(empty($request->pin)){
            $this->JsonResponse['Message'] = 'PIN required!';
        }else{
            $this->JsonResponse['Message'] = "Email not exit.";
            $Query = User::where(['email' => $request->email]);
            if($Query->count() > 0){
                $User = $Query->first();
                if (!Hash::check($request->pin, $User->password)) {
                    $this->JsonResponse['Message'] = "User PIN not match!";
                }else {
                    $this->JsonResponse['Status'] = 200;
                    $this->JsonResponse['Message'] = "Success";
                    $this->JsonResponse['Data'] = $User->toArray();
                    $this->JsonResponse['AuthToken'] = $this->encryptDecrypt($User->id);
                }
            }
        }
        return $this->commonMessage($this->JsonResponse);
    }
}
