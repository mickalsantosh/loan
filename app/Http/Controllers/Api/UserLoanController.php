<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserLoanModel;

class UserLoanController extends Controller {

    function __construct(){
        $this->middleware(function ($request, $next) {
            $CheckAuth = $this->checkUserAuthenticate($request->all());
            if(empty($CheckAuth)){
                return $this->commonMessage($this->JsonResponse);
                exit();
            }
            return $next($request);
        });
    }

    public function applyLoan(Request $request){
        if(empty($request->loan_amount) || !is_numeric($request->loan_amount)){
            $this->JsonResponse['Message'] = 'Loan Amount Required !';
        }elseif(empty($request->loan_period) || !is_numeric($request->loan_period)){
            $this->JsonResponse['Message'] = 'Loan Period Required !';
        }else{
            $Data = array('user_id' => $this->AuthUser['id'], 'loan_amount' => $request->loan_amount, 'loan_period' => $request->loan_period);
            if(UserLoanModel::create($Data)){
                $this->JsonResponse['Status'] = '200';
                $this->JsonResponse['Message'] = 'Loan Applied Successfully!';
            }
        }
        return $this->commonMessage($this->JsonResponse);
    }

    public function applyLoanList(Request $request){
        $this->JsonResponse['Message'] = 'No recode available';
        $this->JsonResponse['Data'] = array();
        if($this->AuthUser['role'] == 1){
            $Query = UserLoanModel::from('user_loan as ul')
                ->join('users as u', 'u.id', '=', 'ul.user_id')
                ->select('ul.id', 'ul.loan_amount', 'ul.loan_period', 'ul.status', 'u.name', 'u.email')
                ->orderBy('ul.id', 'DESC')
                ->get();
        }else{
            $Query = UserLoanModel::select('loan_amount', 'loan_period', 'status')
                ->where('user_id', $this->AuthUser['id'])
                ->orderBy('id', 'DESC')
                ->get();
        }
        if($Query->count() > 0){
            $Data = array();
            foreach ($Query->toArray() as $k => $row){
                $Data[$k] = $row;
                $Data[$k]['status'] = $this->LoanStatus[$row['status']];
            }
            $this->JsonResponse['Status'] = '200';
            $this->JsonResponse['Message'] = 'success';
            $this->JsonResponse['Data'] = $Data;
        }
        return $this->commonMessage($this->JsonResponse);
    }

    public function changeLoanStatus(Request $request, $LoanID=false){
        $this->JsonResponse['Message'] = 'You have no permission to change status';
        if($this->AuthUser['role'] == 1 && !empty($LoanID) && isset($request->status)){
            $Query = UserLoanModel::where('id', $LoanID)->update(['status' => $request->status]);
            if($Query) {
                $this->JsonResponse['Message'] = 'Loan status changed successfully.';
            }
        }
        return $this->commonMessage($this->JsonResponse);
    }
}
