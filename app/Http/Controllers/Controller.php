<?php

namespace App\Http\Controllers;

use http\Env\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\User;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $JsonResponse = array('Status' => 202, 'Message' => 'Something went wrong, Please try again!');
    protected $AuthUser = array();
    protected $LoanStatus = array('0' => 'Pending', '1' => 'Approved', '2' => 'Reject');

    function encryptDecrypt($string, $action = 'e') {
        $str = "1010101010101010";
        $secret_key = bin2hex($str);
        $secret_iv = bin2hex($str);

        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        }else if( $action == 'd' ){
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }
        return $output;
    }

    protected function checkUserAuthenticate($request){
        $Response = false;
        $this->JsonResponse['Message'] = 'Auth Token Required!';
        if(isset($request['AuthToken']) && !empty($request['AuthToken'])){
            $this->JsonResponse['Message'] = 'Auth Token not matched!';
            $DecryptAuthToken = $this->encryptDecrypt($request['AuthToken'], 'd');
            $Query = User::where('id', $DecryptAuthToken);
            if($Query->count() > 0){
                $this->AuthUser = $Query->first()->toArray();
                $this->JsonResponse['Message'] = 'Token matched success.';
                $Response = true;
            }
        }
        return $Response;
    }

    protected function commonMessage($json = array()){
        $content_type = "application/json";
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Expose-Headers: X-API-VERSION");
        header("HTTP/1.1 200");
        header("Content-Type:".$content_type);
        return response()->json($json, $json['Status']);
    }
}
