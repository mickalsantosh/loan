<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLoanModel extends Model
{
    use HasFactory;
    protected $table = 'user_loan';
    protected $primaryKey = 'ID';
    protected $guarded = [];
}
