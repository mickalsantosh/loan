<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLoan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_loan', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->float('loan_amount')->default('0.00');
            $table->tinyInteger('loan_period')->default('3')->comment('In Month');
            $table->tinyInteger('status')->default('0')->comment('0=pending, 1=approved, 2=reject');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_loan');
    }
}
