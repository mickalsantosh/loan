<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('user-register', 'Api\RegistrationController@userRegister');
Route::post('user-login', 'Api\RegistrationController@userLogin');
Route::post('apply-loan', 'Api\UserLoanController@applyLoan');
Route::get('apply-loan-list', 'Api\UserLoanController@applyLoanList');
Route::post('change-loan-status/{loan_id}', 'Api\UserLoanController@changeLoanStatus');
